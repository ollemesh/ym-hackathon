# Приложение школы Node.js «Кошелёк»

## Install

```
npm install
```

MongoDB instance is required to run the app. Load the sample data:

```
mongorestore --db=school-wallet school-wallet-dump
```

Run the app in production:

```
npm run build
npm start
```

## Develop

Run the server:

```
npm run dev
```

In developement mode [Webpack Hot Module Replacement](https://webpack.js.org/concepts/hot-module-replacement/) and [React Hot Loader](https://github.com/gaearon/react-hot-loader) are enabled, no page refresh is required after React components are updated.

When developing API it might be useful to disable Webpack build:

```
npm run dev:api
```

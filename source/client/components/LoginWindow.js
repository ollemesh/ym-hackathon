import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

import {Input, Button} from './';

const Wrapper = styled.div`
	display: flex;
	position: relative;
	height: 100vh;
	width: 100vw;
	align-items: center;
	justify-content: center;
	overflow: hidden;
`;

const Bg = styled.div`
	position: absolute;
	top: -10px;
    bottom: -10px;
    left: -10px;
    right: -10px;
	background-image: url('/assets/login_bg.jpg');
	background-size: cover;
	filter: blur(5px);
`;

const Domik = styled.div`
	width: 240px;
	border-radius: 4px;
	padding: 30px 30px 20px;
	background-color: #353536;
	z-index: 10;
	text-align: center;
`;

const InputField = styled(Input)`
	width: 100%;
	margin-bottom: 10px;
	text-align: center;
`;

const LoginButton = styled(Button)`
	margin: 0 auto;
	display: block;
`;

const ErrorParagraph = styled.p`
	margin-top: 10px;
	color: #ff5f5f;
`;

const Logo = styled.div`
	width: 147px;
	height: 28px;
	margin: 0 auto 10px;
	background-image: url('/assets/yamoney-logo.svg');
`;

class LoginWindow extends Component {
	render() {
		const {error} = this.props;
		return (
			<Wrapper>
				<Bg />
				<Domik>
					<Logo />
					<form action="/users/login" method="POST">
						<InputField
							name='username'
							type='text' />
						<InputField
							name='password'
							type='password' />
						<LoginButton bgColor='#fff'>Войти</LoginButton>
						{error && <ErrorParagraph>{error}</ErrorParagraph>}
					</form>
				</Domik>
			</Wrapper>
		);
	}
}

LoginWindow.propTypes = {
	error: PropTypes.string
};

export default LoginWindow;

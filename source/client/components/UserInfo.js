import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';

import {Button} from './';

const User = styled.div`
	display: flex;
	align-items: center;
	font-size: 15px;
	color: #000;
`;

const Avatar = styled.img`
	width: 42px;
	height: 42px;
	border-radius: 50%;
	margin-right: 10px;
`;

const LogoutButton = styled(Button)`
	margin-left: 10px;
`;
const Info = styled.div``;
const Name = styled.div``;
const Balance = styled.div``;

const UserInfo = (props, context) => {
	const {balance} = context.wallet;
	return (
		<User>
			<Avatar src='/assets/avatar.png' />
			<Info>
				<Balance>{`${balance} ₽`}</Balance>
				<Name>{context.user.name || context.user.username}</Name>
			</Info>
			<LogoutButton onClick={context.onLogout}>Выйти</LogoutButton>
		</User>
	);
};

UserInfo.contextTypes = {
	user: PropTypes.shape({
		username: PropTypes.string.isRequired,
		name: PropTypes.string
	}),
	wallet: PropTypes.shape({
		id: PropTypes.number.isRequired,
		balance: PropTypes.number.isRequired
	}),
	onLogout: PropTypes.func
};


export default UserInfo;

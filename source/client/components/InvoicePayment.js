import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import axios from 'axios';

import InvoicePaymentContract from './InvoicePaymentContract';
import InvoicePaymentSuccess from './InvoicePaymentSuccess';
import InvoicePaymentWait from './InvoicePaymentWait';
import InvoicePaymentBilled from './InvoicePaymentBilled'
import InvoiceHistory from './InvoiceHistory';

const Invoices = styled.div`
	display: flex;
`;

/**
 * Класс компонента InvoicePayment
 */
class InvoicePayment extends Component {
	/**
	 * Конструктор
	 * @param {Object} props свойства компонента InvoicePayment
	 */
	constructor(props) {
		super(props);

		this.state = {stage: 'contract'};
	}

	/**
	 * Обработка успешного платежа
	 * @param {Object} transaction данные о транзакции
	 */
	waitingForPayment({data}) {
		this.props.onInvoiceApproved();
		this.setState({
			stage: 'success',
			invoice: data
		});
	}

	repeatPayment() {
		this.setState({stage: 'contract'});
	}

	/**
	 * проверить общий счет
	 */
	checkPayment(id) {
		axios
			.get(`/invoice/${id}`)
			.then(({data}) => {
				switch (data.status) {
					case 'Approved':
						this.setState({stage: 'approved'});
						break;
					case 'Approving':
						this.setState({stage: 'approving'});
						break;						
					default:
						this.setState({stage: 'canceled'});
						break;
				}		
			});
	}

	payMutual() {
		this.setState({stage: 'billed'});
	}

	payInvoice(item, isApproved=false) {
		let path;
		if(item.details.masterInvoice) path = `/invoice/mutual/${item.id}/pay`;
		else path = `/invoice/${item.id}/pay`;
		const data = {
			isApproved,
			details: 'ответ'
		};
		axios
			.post(path, data)
			.then(({data}) => {
				this.setState({stage: 'billed'});		
			});
	}

	/**
	 * Рендер компонента
	 *
	 * @override
	 * @returns {JSX}
	 */
	render() {
		const {invoiceHistory} = this.props;
		switch (this.state.stage) {
			case 'success':
				return (
					<Invoices>
						<InvoicePaymentSuccess
							invoice={this.state.invoice}
							checkPayment={(id) => this.checkPayment(id)} />
						<InvoiceHistory 
							payInvoice={(item, isApproved) => this.payInvoice(item, isApproved)}
							invoiceHistory={invoiceHistory} />
					</Invoices>
				);
			case 'approved':
			case 'approving':
			case 'canceled':
				return (
					<Invoices>
						<InvoicePaymentWait
							status={this.state.stage}
							invoice={this.state.invoice}
							checkPayment={(id) => this.checkPayment(id)} 
							pay ={() => this.payMutual()}/>
						<InvoiceHistory 
							payInvoice={(item, isApproved) => this.payInvoice(item, isApproved)}
							invoiceHistory={invoiceHistory} />
					</Invoices>
				);
			case 'billed':
			return (
					<Invoices>
						<InvoicePaymentBilled
							invoice={this.state.invoice}
							repeatPayment={(id) => this.repeatPayment(id)}/>							
						<InvoiceHistory 
							payInvoice={(item, isApproved) => this.payInvoice(item, isApproved)}							
							invoiceHistory={invoiceHistory} />
					</Invoices>
				);

			default:
				return (
					<Invoices>
						<InvoicePaymentContract
							waitingForPayment={(invoice) => this.waitingForPayment(invoice)} />
						<InvoiceHistory 
							payInvoice={(item, isApproved) => this.payInvoice(item, isApproved)}							
							invoiceHistory={invoiceHistory} />
					</Invoices>
				);
				break;
		}
		
	}
}

InvoicePayment.propTypes = {
	invoiceHistory: PropTypes.arrayOf(PropTypes.object).isRequired,
	onInvoiceApproved: PropTypes.func.isRequired
};

export default InvoicePayment;

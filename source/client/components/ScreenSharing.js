/* eslint no-console: "off" */
import {Component} from 'react';
import PropTypes from 'prop-types';
import unique from 'unique-selector';

const screenSharingUser = (name) => {
	// Get URL for WS connection from current location
	const getWSUrl = () => {
		const loc = window.location;
		const protocol = loc.protocol === 'https:' ? 'wss:' : 'ws:';
		return `${protocol}//${loc.host}/`;
	};

	let ws;

	// Data collection
	const observer = new MutationObserver(() => {
		ws.send(JSON.stringify({
			type: 'USER_SCREEN_UPDATE',
			data: {
				html: document.documentElement.outerHTML
			}
		}));
	});

	const onMouseMove = (e) => {
		ws.send(JSON.stringify({
			type: 'USER_MOUSE_MOVE',
			data: {
				x: e.pageX,
				y: e.pageY
			}
		}));
	};

	const onMouseDown = () => {
		ws.send(JSON.stringify({
			type: 'USER_MOUSE_DOWN'
		}));
	};

	const onMouseUp = () => {
		ws.send(JSON.stringify({
			type: 'USER_MOUSE_UP'
		}));
	};

	const onScroll = (e) => {
		if (e.target === document) {
			ws.send(JSON.stringify({
				type: 'USER_SCROLL_DOCUMENT',
				data: {
					x: window.pageXOffset,
					y: window.pageYOffset
				}
			}));
		} else {
			ws.send(JSON.stringify({
				type: 'USER_SCROLL_ELEMENT',
				data: {
					selector: unique(e.target),
					x: e.target.scrollLeft,
					y: e.target.scrollTop
				}
			}));
		}
	};

	// Sharing toggling
	const startSharing = () => {
		observer.observe(document.body, {
			childList: true,
			attributes: true,
			characterData: true,
			subtree: true
		});
		document.addEventListener('mousemove', onMouseMove);
		document.addEventListener('mousedown', onMouseDown);
		document.addEventListener('mouseup', onMouseUp);
		document.addEventListener('scroll', onScroll, true);
	};

	const stopSharing = () => {
		observer.disconnect();
		document.removeEventListener('mousemove', onMouseMove);
		document.removeEventListener('mousedown', onMouseDown);
		document.removeEventListener('mouseup', onMouseUp);
		document.removeEventListener('scroll', onScroll, true);
	};

	// One-time messages
	const sendInitialScreen = () => {
		ws.send(JSON.stringify({
			type: 'USER_SCREEN_INITIAL',
			data: {
				screenSize: {
					width: document.documentElement.clientWidth,
					height: document.documentElement.clientHeight,
					vScroll: window.innerWidth > document.documentElement.clientWidth,
					hScroll: window.innerHeight > document.documentElement.clientHeight
				},
				html: document.documentElement.outerHTML
			}
		}));
	};

	// Message processing
	const onMessage = (event) => {
		const {type} = JSON.parse(event.data);
		switch (type) {
			case 'ADMIN_SUBSCRIBE_TO_USER':
				sendInitialScreen();
				startSharing();
				break;
			case 'ADMIN_UNSUBSCRIBE_FROM_USER':
				stopSharing();
				break;
			default:
				console.error(`Messages of type ${type} are not supported`);
		}
	};

	return {
		start() {
			// Create WebSocket connection
			ws = new WebSocket(`${getWSUrl()}?name=${name}`);

			ws.addEventListener('message', console.log);
			ws.addEventListener('message', onMessage);
			ws.addEventListener('open', () => console.log('Connected'));
			ws.addEventListener('close', () => console.log('Disconnected'));
			ws.addEventListener('error', console.error);
		},
	};
};

class ScreenSharing extends Component {
	componentDidMount() {
		this.sharing = screenSharingUser(this.props.name);
		this.sharing.start();
	}

	render() {
		return null;
	}
}

ScreenSharing.propTypes = {
	name: PropTypes.string.isRequired
};

export default ScreenSharing;

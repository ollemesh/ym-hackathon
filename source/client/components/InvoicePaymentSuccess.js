import React from 'react';
import styled from 'react-emotion';
import PropTypes from 'prop-types';

import {Island} from './';

const InvoicePaymentLayout = styled(Island)`
	width: 390px;
	background: #2A628D;
	position: relative;
	color: #fff;
`;

const SuccessIcon = styled.div`
	width: 48px;
	height: 48px;
	background-image: url(/assets/round-check.svg);
	position: absolute;
	top: 27;
	right: 32;
`;

const Header = styled.div`
	font-size: 24px;
`;

const Sum = styled.div`
	font-size: 48px;
`;

// const CommissionTips = styled.div`
// 	font-size: 13px;
// 	opacity: 0.6;
// 	margin-bottom: 20px;
// `;

const Section = styled.div`
	position: relative;
	padding-left: 160px;
	margin-bottom: 20px;
`;

const SectionLabel = styled.div`
	font-size: 15px;
	position: absolute;
	left: 0px;
`;

const SectionValue = styled.div`
	font-size: 15px;
`;

const Instruction = styled.div`
	margin-bottom: 40px;
	font-size: 15px;
`;

const CheckPayment = styled.button`
	font-size: 13px;
	background-color: rgba(0, 0, 0, 0.08);
	height: 42px;
	display: flex;
	justify-content: center;
	align-items: center;
	border: none;
	width: 100%;
	position: absolute;
	left: 0;
	bottom: 0;
	cursor: pointer;
	text-transform: uppercase;
`;

const InvoicePaymentSuccess = ({invoice, checkPayment}) => {
	const {sum, details, id} = invoice;
	const userStr = details.users.join(', ');
	return (
		<InvoicePaymentLayout>
			<SuccessIcon />
			<Header>Общий счет создан!</Header>
			<Section>
				<SectionLabel>Номер счета</SectionLabel>
				<SectionValue>{id}</SectionValue>
			</Section>
			<Section>
				<SectionLabel>Друзья в деле</SectionLabel>
				<SectionValue>{userStr}</SectionValue>
			</Section>
			<Instruction>
				Дождитесь подтвержения участия от друзей
			</Instruction>
			<CheckPayment onClick={() => checkPayment(id)}>Проверить состояние</CheckPayment>
		</InvoicePaymentLayout>
	);
};

InvoicePaymentSuccess.propTypes = {
	invoice: PropTypes.shape({
		sum: PropTypes.number,
		details: PropTypes.object,
		id: PropTypes.number
	}).isRequired,
	checkPayment: PropTypes.func.isRequired
};

export default InvoicePaymentSuccess;

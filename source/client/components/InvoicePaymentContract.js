import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import axios from 'axios';

import {Island, Title, Button, Input} from './';

const InvoicePaymentLayout = styled(Island)`
	width: 390px;
	background: #2A628D;
`;

const InvoicePaymentTitle = styled(Title)`
	color: #fff;
`;

const InputField = styled.div`
	display: flex;
	align-items: center;
	margin-bottom: 26px;
	position: relative;
	padding-left: 150px;
`;

const Label = styled.div`
	font-size: 15px;
	color: #fff;
	position: absolute;
	left: 0;
`;

const Currency = styled.span`
	font-size: 13px;
	color: #fff;
	margin-left: 12px;
`;

const Underline = styled.div`
	height: 1px;
	margin-bottom: 20px;
	background-color: rgba(0, 0, 0, 0.16);
`;

const PaymentButton = styled(Button)`
	float: right;
`;

const InputUsers = styled(Input)`
	width: 225px;
`;

const InputDetails = styled(Input)`
width: 405px;
`;


const InputSum = styled(Input)`
	width: 160px;
`;

const InputCommision = styled(Input)`
	cursor: no-drop;
	width: 160px;
	border: dotted 1.5px rgba(0, 0, 0, 0.2);
	background-color: initial;
`;

/**
 * Компонент InvoicePaymentContract
 */
class InvoicePaymentContract extends Component {
	/**
	 * Конструктор
	 * @param {Object} props свойства компонента InvoicePaymentContract
	 */
	constructor(props) {
		super(props);

		this.state = {
			users: ['vitalyq' , 'nik'],
			sum: 1000,
			details: 'Давайте скинемся на др',
			type: 'mutual'
		};
	}

	/**
	 * Получить цену с учетом комиссии
	 * @returns {Number}
	 */
	getSumChuncks() {
		const {sum, users} = this.state;

		const isNumber = !isNaN(parseFloat(sum)) && isFinite(sum);
		if (!isNumber || sum <= 0) {
			return 0;
		}
		return Math.ceil(Number(sum/users.length));
	}

	/**
	 * Отправка формы
	 * @param {Event} event событие отправки формы
	 */
	onSubmitForm(event) {
		if (event) {
			event.preventDefault();
		}

		const {sum, users, type, details} = this.state;

		const isNumber = !isNaN(parseFloat(sum)) && isFinite(sum);
		if (!isNumber || sum === 0) {
			return;
		}
		
		axios
			.post(`/invoice/mutual/`, {sum, users, type, details})
			.then((data) => this.props.waitingForPayment(data));
	}

	onUsersInputValueChange(event){
		if (!event) {
			return;
		}
		const {name, value} = event.target;
		this.setState({
			[name]: value.split(',')
		});
	}

	onDetailsInputValueChange(event) {
		if (!event) {
			return;
		}
		const {name, value} = event.target;
		
		this.setState({
			[name]: value,
		});
	}

	/**
	 * Обработка изменения значения в input
	 * @param {Event} event событие изменения значения input
	 */
	onChangeInputValue(event) {
		if (!event) {
			return;
		}

		const {name, value} = event.target;

		this.setState({
			[name]: value
		});
	}

	/**
	 * Рендер компонента
	 *
	 * @override
	 * @returns {JSX}
	 */
	render() {
		const {commission} = this.state;

		return (
			<InvoicePaymentLayout>
				<form onSubmit={(event) => this.onSubmitForm(event)}>
					<InvoicePaymentTitle>Общий счет</InvoicePaymentTitle>
					<InputField>
						<Label>Друзья</Label>
						<InputUsers
							name='users'
							value={this.state.users}
							onChange={(event) => this.onUsersInputValueChange(event)} />
					</InputField>
					<InputField>
						<Label>Сумма</Label>
						<InputSum
							name='sum'
							value={this.state.sum}
							onChange={(event) => this.onChangeInputValue(event)} />
						<Currency>₽</Currency>
					</InputField>
					<InputField>
						<Label>Скидываемся по</Label>
						<InputCommision value={this.getSumChuncks()} />
						<Currency>₽</Currency>
					</InputField>
					<InputField>
						<Label>Комментарии</Label>
						<InputDetails
						 name='details'
						 onChange={(event) => this.onDetailsInputValueChange(event)}/>
					</InputField>
					<Underline />
					<PaymentButton bgColor='#fff' textColor='#2A628D'>Предложить</PaymentButton>
				</form>
			</InvoicePaymentLayout>
		);
	}
}

InvoicePaymentContract.propTypes = {
	waitingForPayment: PropTypes.func.isRequired
};

export default InvoicePaymentContract;

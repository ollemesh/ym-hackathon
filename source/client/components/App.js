import React, {Component} from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import {injectGlobal} from 'emotion';
import CardInfo from 'card-info';
import axios from 'axios';

import {
	CardsBar,
	Header,
	History,
	Prepaid,
	MobilePayment,	
	Withdraw,
	ScreenSharing,
	LoginWindow,
	InvoicePayment
} from './';

import './fonts.css';

injectGlobal([`
	html,
	body {
		margin: 0
	}

	#root {
		height: 100%
		font-family: 'Open Sans'
		color: #000
	}
`]);

const Wallet = styled.div`
	display: flex;
	min-height: 100%;
	background-color: #fcfcfc;
`;

const CardPane = styled.div`
	flex-grow: 1;
`;

const Workspace = styled.div`
	display: flex;
	flex-wrap: wrap;
	max-width: 970px;
	padding: 15px;
`;

/**
 * Приложение
 */
class App extends Component {
	/**
	 * Подготавливает данные карт
	 *
	 * @param {Object} cards данные карт
	 * @returns {Object[]}
	 */
	static prepareCardsData(cards) {
		return cards.map((card) => {
			const cardInfo = new CardInfo(card.cardNumber, {
				banksLogosPath: '/assets/',
				brandsLogosPath: '/assets/'
			});

			return {
				id: card.id,
				balance: card.balance,
				number: cardInfo.numberNice,
				bankName: cardInfo.bankName,
				theme: {
					bgColor: cardInfo.backgroundColor,
					textColor: cardInfo.textColor,
					bankLogoUrl: cardInfo.bankLogoSvg,
					brandLogoUrl: cardInfo.brandLogoSvg,
					bankSmLogoUrl: `/assets/${cardInfo.bankAlias}-history.svg`
				}
			};
		});
	}

	static prepareHistory(cardsList, transactionsData) {
		return transactionsData.map((data) => {
			const card = cardsList.find((item) => item.id === Number(data.cardId));
			return card ? Object.assign({}, data, {card}) : data;
		});
	}

	static prepareInvoices(invoicesData) {
		
		return invoicesData.map((invoice) => {
			return {           
				id: invoice.id,
				type: invoice.type || 'default',
				status: invoice.status,
				time: invoice.created_at || invoice.time,
				sum: invoice.sum,
				details: invoice.details     
			}
		});
	}

	static prepareUserWalletInfo(wallet){
		return {
			id: wallet.id,
			balance: wallet.balance
		}
	}

	/**
	 * Конструктор
	 */
	constructor(props) {
		super();

		const {data} = props;
		const cardsList = App.prepareCardsData(data.cards);
		const cardHistory = App.prepareHistory(cardsList, data.transactions);
		const invoices = App.prepareInvoices(data.invoices);
		const wallet = App.prepareUserWalletInfo(data.wallet);
		this.state = {
			user: data.user,
			error: data.error,
			cardsList,
			cardHistory,
			invoices,
			activeCardIndex: 0,
			removeCardId: 0,
			isCardRemoving: false,
			isCardsEditable: false,
			wallet
		};
	}

	getChildContext() {
		return {
			wallet: this.state.wallet,
			user: this.state.user,
			onLogin: this.onLogin.bind(this),
			onLogout: this.onLogout.bind(this)
		};
	}

	onLogin(username, password) {
		return axios.post('/users/login', {username, password}).then(({data}) => {
			this.setState({
				user: {
					username: data.username,
					name: data.name,
					role: data.role
				}
			});
		});
	}

	onLogout() {
		axios.get('/users/logout').then(() => {
			this.setState({user: null});
		}).catch((err) => {
			console.error(err);
		});
	}

	onLogout() {
		axios.get('/users/logout').then(() => {
			this.setState({user: null});
		}).catch((err) => {
			console.error(err);
		});
	}
	
	/**
	 * Обработчик переключения карты
	 *
	 * @param {Number} activeCardIndex индекс выбранной карты
	 */
	onCardChange(activeCardIndex) {
		this.setState({activeCardIndex});
	}

	/**
	* Обработчик события редактирования карт
	* @param {Boolean} isEditable Признак редактируемости
	*/
	onEditChange(isEditable) {
		const isCardsEditable = !isEditable;
		this.setState({
			isCardsEditable,
			isCardRemoving: false
		});
	}

	/**
	* Функция вызывает при успешной транзакции
	*/
	onTransaction() {
		axios.get('/cards').then(({data}) => {
			const cardsList = App.prepareCardsData(data);
			this.setState({cardsList});

			axios.get('/transactions').then(({data}) => {
				const cardHistory = App.prepareHistory(cardsList, data);
				this.setState({cardHistory});
			});
		});
	}

	onInvoiceApproved() {

		axios.get('/invoice').then(({data}) => {
			const invoices = App.prepareInvoices(data);
			this.setState({invoices});
		});
		axios.get('/wallet').then(({data}) => {
			const wallet = App.prepareUserWalletInfo(data);
			this.setState({wallet});
		});
	}

	/**
	 * Обработчик события переключения режима сайдбара
	 * @param {String} mode Режим сайдбара
	 * @param {String} index Индекс выбранной карты
	 */
	onChangeBarMode(event, removeCardId) {
		event.stopPropagation();
		this.setState({
			isCardRemoving: true,
			removeCardId
		});
	}

	/**
	 * Удаление карты
	 * @param {Number} index Индекс карты
	 */
	deleteCard(id) {
		axios
			.delete(`/cards/${id}`)
			.then(() => {
				axios.get('/cards').then(({data}) => {
					const cardsList = App.prepareCardsData(data);
					this.setState({cardsList});
				});
			});
	}

	/**
	 * Рендер компонента
	 *
	 * @override
	 * @returns {JSX}
	 */
	render() {
		const {
			cardsList,
			activeCardIndex,
			cardHistory,
			invoices,
			wallet,
			isCardsEditable,
			isCardRemoving,
			removeCardId,
			user,
			error
		} = this.state;
		
		if (!user) {
			return <LoginWindow error={error} />;
		}

		const activeCard = cardsList[activeCardIndex] || {};

		const inactiveCardsList = cardsList.filter((card, index) => (index === activeCardIndex ? false : card));
		const filteredHistory = cardHistory.filter((data) => (
			Number(data.cardId) === Number(activeCard.id)
		));
		
		return (
			<Wallet>
				<CardsBar
					activeCardIndex={activeCardIndex}
					removeCardId={removeCardId}
					cardsList={cardsList}
					onCardChange={(index) => this.onCardChange(index)}
					isCardsEditable={isCardsEditable}
					isCardRemoving={isCardRemoving}
					deleteCard={(index) => this.deleteCard(index)}
					onChangeBarMode={(event, index) => this.onChangeBarMode(event, index)} />
				<CardPane>
					<Header activeCard={activeCard} wallet={wallet} />
					<Workspace>
						<History cardHistory={filteredHistory} />
						<Prepaid
							activeCard={activeCard}
							inactiveCardsList={inactiveCardsList}
							onCardChange={(newActiveCardIndex) => this.onCardChange(newActiveCardIndex)}
							onTransaction={() => this.onTransaction()} />
						<MobilePayment activeCard={activeCard} onTransaction={() => this.onTransaction()} />
						<Withdraw
							activeCard={activeCard}
							inactiveCardsList={inactiveCardsList}
							onTransaction={() => this.onTransaction()} />
						<InvoicePayment invoiceHistory={invoices} onInvoiceApproved={() => this.onInvoiceApproved()} />						
					</Workspace>
				</CardPane>
				<ScreenSharing name={`${user.name} (${user.username})`} />
			</Wallet>
		);
	}
}

App.propTypes = {
	data: PropTypes.shape({
		user: PropTypes.object,
		cards: PropTypes.array,
		transactions: PropTypes.array
	})
};

App.childContextTypes = {
	user: PropTypes.shape({
		username: PropTypes.string.isRequired,
		name: PropTypes.string
	}),
	onLogin: PropTypes.func,
	onLogout: PropTypes.func,
	wallet: PropTypes.shape({
		id: PropTypes.number.isRequired,
		balance: PropTypes.number.isRequired
	})
};

export default App;

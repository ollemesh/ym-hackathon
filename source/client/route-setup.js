/* eslint global-require: "off", import/no-unresolved: "off" */
// In order for development build to work, run the production build once
// to copy the assets.
const router = require('koa-router')();

const isProduction = process.env.NODE_ENV === 'production';
const renderClient = process.env.NO_CLIENT !== '1';

const getData = async (ctx) => {
	const {user} = ctx.passport;
	let error;
	let cards = [];
	let transactions = [];
	let invoices = [];
	let wallet = {};

	if (user) {
		cards = await ctx.cardService.getAllByUser(user._id);
		transactions = await ctx.transactionService.getAll();
		invoices = await ctx.payService.getAll(user._id);
		wallet = await ctx.cardService.getByUser(user._id);
	}

	if (ctx.session && ctx.session.flash && ctx.session.flash.message) {
		error = ctx.session.flash.message;
	}

	return {
		user,
		error,
		cards,
		transactions,
		invoices,
		wallet
	};
};

module.exports = (app) => {
	if (isProduction) {
		const serverRenderer = require('../../public/index.server.js').default;
		router.get('/', serverRenderer({getData}));
		app.use(router.routes());
	} else if (renderClient) {
		const webpack = require('webpack');
		const koaWebpack = require('koa-webpack');
		const hotServerMiddleware = require('webpack-hot-server-middleware');
		const config = require('../../webpack.config');

		const compiler = webpack(config);
		const devHot = koaWebpack({
			compiler,
			dev: {
				publicPath: '/',
				serverSideRender: true,
				stats: {
					colors: true
				},
			},
		});
		const hotServer = hotServerMiddleware(compiler, {
			createHandler: hotServerMiddleware.createKoaHandler,
			serverRendererOptions: {
				getData
			},
		});

		// Render view from the server bundle
		router.get('/', hotServer);
		app.use(devHot);
		app.use(router.routes());
	}
};

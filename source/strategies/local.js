const LocalStrategy = require('passport-local').Strategy;

module.exports = (model) => (
	new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password'
	}, async (username, password, done) => {
		try {
			const user = await model.getBy({username, password});

			if (!user) {
				return done(null, false);
			}

			return done(null, user);
		} catch (e) {
			return done(e);
		}
	})
);

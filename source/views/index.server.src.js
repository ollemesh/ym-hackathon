/* eslint react/no-danger: "off" */
import React from 'react';
import {renderToString, renderToStaticMarkup} from 'react-dom/server';
import {extractCritical} from 'emotion-server';
import {App} from '../client/components';

export default function serverRenderer(options) {
	// Do not pass client and server stats to the view.
	const data = Object.assign({}, options);
	delete data.clientStats;
	delete data.serverStats;

	return async (ctx) => {
		const appData = await data.getData(ctx);
		const app = renderToString(<App data={appData} />);
		const {html, ids, css} = extractCritical(app);
		const viewData = `window.__data=${JSON.stringify({ids, appData})};`;

		const template = (
			<html lang='ru'>
				<head>
					<meta charSet='utf-8' />
					<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no' />
					<meta name='description' content='Кошелёк' />
					<title>Кошелёк</title>
					<link rel='stylesheet' href='/index.css' />
					<style dangerouslySetInnerHTML={{__html: css}} />
				</head>
				<body>
					<div id='root' dangerouslySetInnerHTML={{__html: html}} />
					<script dangerouslySetInnerHTML={{__html: viewData}} />
					<script src='/index.js' />
				</body>
			</html>
		);

		ctx.body = renderToStaticMarkup(template);
	};
}

// HTML sanitization setup
// This setup is not sufficient for production use:
// it is possible to embed resources from third-party hosts.
const sanitizeHtml = require('sanitize-html');

// Secure attribute whitelist inspired by:
// https://help.webex.com/docs/DOC-6435
const allowedAttributes = {
	'*': [
		'abbr', 'accesskey', 'align', 'alt', 'autocomplete', 'axis',
		'background', 'bgcolor', 'border', 'cellpadding', 'cellspacing',
		'char', 'charoff', 'class', 'cols', 'colspan', 'disabled', 'headers',
		'height', 'id', 'lang', 'leftmargin', 'marginheight', 'marginwidth',
		'media', 'name', 'nowrap', 'readonly', 'rows', 'rowspan', 'scope', 'shape',
		'size', 'style', 'title', 'topmargin', 'valign', 'width', 'value'
	],
	link: ['rel', 'href'],
	img: ['src']
};

const elemBlacklist = {
	script: true,
	base: true,
	plaintext: true
};

const exclusiveFilter = (frame) => elemBlacklist[frame.tag];

// Mask card numbers.
// This feature should be reimplemented with masking markup.
const textFilter = (text) => text
	.replace(/^\d{4} \d{4} \d{4} (\d{3})$/, 'XXXX XXXX XXXX $1')
	.replace(/^-?\d+ ₽$/, 'XXX ₽');

module.exports = (dirty) => sanitizeHtml(dirty, {
	allowedTags: false,
	allowedAttributes,
	exclusiveFilter,
	textFilter
});

const WebSocket = require('ws');
const uuidv4 = require('uuid/v4');
const url = require('url');
const sanitize = require('./sanitize');

const ADMIN_PASSWORD = 'W0uyXZ5zLB4hVJ8dSM4h';
const users = {};
let adminSocket;
let selectedUser;

// To simplify serialization:
// save socket on user object with symbol key
const wsSymbol = Symbol('ws');

// Data sending helpers
const sendData = (socket, data) => {
	const payload = typeof data === 'string' ?
		data : JSON.stringify(data);
	socket.send(payload);
};

const sendToAdmin = (data) => {
	if (adminSocket && adminSocket.readyState === WebSocket.OPEN) {
		sendData(adminSocket, data);
	}
};

const sendToUser = (data) => {
	if (!selectedUser) { return; }
	const selectedUserData = users[selectedUser];
	if (!selectedUserData) { return; }
	const userSocket = selectedUserData[wsSymbol];
	if (userSocket.readyState === WebSocket.OPEN) {
		sendData(userSocket, data);
	}
};

// Authorize admin by password during handshake
const verifyClient = ({req}) => {
	const {password} = url.parse(req.url, true).query;
	return !password || password === ADMIN_PASSWORD;
};

module.exports = (server) => {
	const wss = new WebSocket.Server({server, verifyClient});

	wss.on('connection', (ws, req) => {
		const userId = uuidv4();
		const {name, password} = url.parse(req.url, true).query;
		const isAdmin = Boolean(password);
		const isUser = !isAdmin;

		// If admin has connected, send her a list of users
		if (isAdmin) {
			if (!adminSocket || adminSocket.readyState !== WebSocket.OPEN) {
				adminSocket = ws;
			}
			sendToAdmin({
				type: 'ADMIN_USER_LIST',
				data: users
			});
		}

		// If user has connected, add her to the collection
		if (isUser) {
			users[userId] = {
				name,
				[wsSymbol]: ws,
			};
			sendToAdmin({
				type: 'ADMIN_USER_LIST',
				data: users
			});
		}

		ws.on('message', (message) => {
			const {type, data} = JSON.parse(message);
			switch (type) {
				case 'USER_SCREEN_INITIAL':
				case 'USER_SCREEN_UPDATE':
					if (userId !== selectedUser) { return; }
					data.html = sanitize(data.html);
					sendToAdmin({type, data});
					break;
				case 'USER_MOUSE_MOVE':
				case 'USER_MOUSE_DOWN':
				case 'USER_MOUSE_UP':
				case 'USER_SCROLL_DOCUMENT':
				case 'USER_SCROLL_ELEMENT':
					if (userId !== selectedUser) { return; }
					sendToAdmin(message);
					break;
				case 'ADMIN_SUBSCRIBE_TO_USER':
					if (!isAdmin) { return; }
					// Notify last user, update selected user, notify new user
					sendToUser({
						type: 'ADMIN_UNSUBSCRIBE_FROM_USER'
					});
					selectedUser = data;
					sendToUser({
						type: 'ADMIN_SUBSCRIBE_TO_USER'
					});
					break;
				default:
			}
		});

		ws.on('close', () => {
			// Remove user from collection
			// Send new user list to admin
			if (isUser) {
				delete users[userId];
				sendToAdmin({
					type: 'ADMIN_USER_LIST',
					data: users
				});
			}

			// Notify user about admin disconnect
			if (isAdmin) {
				sendToUser({
					type: 'ADMIN_UNSUBSCRIBE_FROM_USER'
				});
			}
		});
	});
};

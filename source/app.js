'use strict';

const fs = require('fs');
const http = require('http');
const https = require('https');
const Koa = require('koa');
const serve = require('koa-static');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser')();
const passport = require('koa-passport');
const session = require('koa-session');
const MongooseStore = require('koa-session-mongoose');

require('dotenv').config();

const {
	PORT = 3000,
	DB_HOST = 'mongodb://localhost/school-wallet',
	NODE_HTTPS = false,
} = process.env;

const logger = require('libs/logger')('app');

const {renderToStaticMarkup} = require('react-dom/server');

const getCardsController = require('./controllers/cards/get-cards');
const createCardController = require('./controllers/cards/create');
const deleteCardController = require('./controllers/cards/delete');
const getTransactionController = require('./controllers/transactions/get');
const createTransactionsController = require('./controllers/transactions/create');
const cardToCard = require('./controllers/cards/card-to-card');
const cardToMobile = require('./controllers/cards/card-to-mobile');
const mobileToCard = require('./controllers/cards/mobile-to-card');
const getTransactionsController = require('./controllers/transactions/get-transactions');
const setupClientRoute = require('./client/route-setup');
const setupScreenSharing = require('./screen-sharing/init');

const loginUser = require('./controllers/users/login');
const logoutUser = require('./controllers/users/logout');
const createUser = require('./controllers/users/create');
const getUser = require('./controllers/users/get');

const getWalletController = require('./controllers/cards/get-wallet');

const createMutualInvoice = require('./controllers/invoices/mutual/create');
const payMutualInvoice = require('./controllers/invoices/mutual/pay');
const createInvoice = require('./controllers/invoices/create');
const payInvoice = require('./controllers/invoices/pay');
const getInvoices = require('./controllers/invoices/get');
const getInvoice = require('./controllers/invoices/get-by-id');

const errorController = require('./controllers/error');

const ApplicationError = require('libs/application-error');

const cardService = require('source/services/cardService');
const transactionService = require('source/services/transactionService');
const payService = require('source/services/payService');

const UsersModel = require('source/models/users')
const Users = new UsersModel();
const userService = require('source/services/userService')(Users);

const LocalStrategy = require('source/strategies/local');

const mongoose = require('mongoose');

mongoose.connect(DB_HOST, {useMongoClient: true}, (err) => {
	if (err) {
		logger.error('Unable to connect to the server. Error:', err);
	} else {
		logger.info('Connected to Server successfully!');
	}
});

mongoose.Promise = global.Promise;

const app = new Koa();

// passport handling
const sessionConfig = {
	key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
	/** (number || 'session') maxAge in ms (default is 1 days) */
	/** 'session' will result in a cookie that expires when session/browser is closed */
	/** Warning: If a session cookie is stolen, this cookie will never expire */
	maxAge: 86400000,
	overwrite: true, /** (boolean) can overwrite or not (default true) */
	httpOnly: true, /** (boolean) httpOnly or not (default true) */
	signed: true, /** (boolean) signed or not (default true) */
	rolling: true,
	// store: new MongooseStore({
	// 	collection: 'sessions',
	// 	connection: mongoose.connection
	// })
};

app.keys = ['some secret hurr'];

app.use(session(sessionConfig, app));

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done) => {
	done(null, user._id); //eslint-disable-line
});

passport.deserializeUser(async (id, done) => {
	const user = await Users.getBy({_id: id});
	done(null, user);
});

passport.use(LocalStrategy(Users));

app.use(async (ctx, next) => {
	ctx.flash = (type, message) => {
		ctx.session.flash = {type, message};
	};

	await next();
});

router.post('/users/create', createUser);
router.post('/users/login', passport.authenticate('local', {
	successRedirect: '/',
	failureRedirect: '/',
	failureFlash: true
}));
router.get('/users/logout', logoutUser);
router.get('/users/:username', getUser);
// passport handling

// Сохраним параметр id в ctx.params.id
router.param('id', (id, ctx, next) => next());

router.get('/cards/', getCardsController);
router.post('/cards/', createCardController);
router.delete('/cards/:id', deleteCardController);

router.get('/cards/:id/transactions/', getTransactionController);
router.post('/cards/:id/transactions/', createTransactionsController);

router.post('/cards/:id/transfer', cardToCard);
router.post('/cards/:id/pay', cardToMobile);
router.post('/cards/:id/fill', mobileToCard);

router.get('/transactions/', getTransactionsController);
router.get('/wallet/', getWalletController);

router.get('/invoice/', getInvoices);
router.get('/invoice/:id', getInvoice);
router.post('/invoice/', createInvoice);
router.post('/invoice/:id/pay', payInvoice);
router.post('/invoice/mutual/', createMutualInvoice);
router.post('/invoice/mutual/:id/pay', payMutualInvoice);

router.all('/error', errorController);

// logger
app.use(async (ctx, next) => {
	const start = new Date();
	await next();
	const ms = new Date() - start;
	logger.info(`${ctx.method} ${ctx.url} - ${ms}ms`);
});

// error handler
app.use(async (ctx, next) => {
	try {
		await next();
	} catch (err) {
		logger.error('Error detected', err);
		ctx.status = err instanceof ApplicationError ? err.status : 500;
		ctx.body = `Error [${err.message}] :(`;
	}
});

// Создадим модель Cards и Transactions на уровне приложения и проинициализируем ее
app.use(async (ctx, next) => {
	ctx.cardService = cardService;
	ctx.transactionService = transactionService;
	ctx.userService = userService;
	ctx.payService = payService;
	await next();
});


app.use(bodyParser);
app.use(router.routes());
setupClientRoute(app);
app.use(serve('./public'));

const listenCallback = function() {
	const {
		port
	} = this.address();

	logger.info(`Application started on ${port}`);
};

if (module.parent) {
	module.exports = app;
} else {
	let server = null;

	if (!NODE_HTTPS) {
		server = http.createServer(app.callback());
	} else {
		const protocolSecrets = {
			key: fs.readFileSync('fixtures/key.key'),
			cert: fs.readFileSync('fixtures/cert.crt')
		};

		server = https.createServer(protocolSecrets, app.callback());
	}

	setupScreenSharing(server);
	server.listen(PORT, listenCallback);
}

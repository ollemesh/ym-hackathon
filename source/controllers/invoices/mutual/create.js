'use strict';

const ApplicationError = require('libs/application-error');
const Joi = require('joi');

const joiConfig = {
	allowUnknown: true,
	abortEarly: false
}
const schema = {
	sum: Joi.number().required().positive(),
	details: Joi.string(),
	type: Joi.string().required().default('mutual'),
	users: Joi.array().items(Joi.string()).max(5) // !!
};

module.exports = async (ctx) => {
	if (!ctx.isAuthenticated()) {
		throw new ApplicationError('Access denied', 403);
	}

	const data = ctx.request.body;
	const {user} = ctx.passport;
	// if(data.users)
	// const users = data.users.split(',')

	const {
		error,
		value
	} = Joi.validate(data, schema, joiConfig);
	if (error) {
		ctx.status = 404;
		ctx.body = error;
		return;
	}

	const users = [];
	for (let i = 0; i < value.users.length; i++) {
		const iUser = await ctx.userService.get(value.users[i].trim());
		users.push(iUser._id);
	}

	const usersNames = value.users;
	value.users = users;
	value.user = user._id;
	value.reciever = user._id;
	const newInvoice = await ctx.payService.create(value);

	newInvoice.details.users = usersNames;
	newInvoice.created_at = new Date();
	ctx.status = 201;
	ctx.body = newInvoice;
};

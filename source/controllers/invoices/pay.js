// в последствии нужно добавить двойной апрув. Когда приходит инвой и кода проходит оплата.

const ApplicationError = require('libs/application-error');

const Joi = require('joi');
const joiConfig = {
	allowUnknown: true,
	abortEarly: false
};
const schema = {
	invoiceNumber: Joi.number().required(),
	isApproved: Joi.boolean().required().default(false),
	details: Joi.string()
};

module.exports = async (ctx) => {
	const data = ctx.request.body;
	const invoiceId = parseInt(ctx.params.id, 10);
	if (!invoiceId) throw new ApplicationError(`No card with id ${invoiceId}`, 404);
	data.invoiceNumber = invoiceId;

	const {
		error,
		value
	} = Joi.validate(data, schema, joiConfig);

	if (error) {
		ctx.status = 404;
		ctx.body = error;
		return;
	}

	const result = await ctx.payService.payInvoice(value);
	ctx.status = 201;
	ctx.body = result;
};

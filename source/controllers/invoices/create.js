'use strict';
const Joi = require('joi');
const joiConfig = {
	allowUnknown: true,
	abortEarly: false
}
const schema = {
	sum: Joi.number().required().positive(),
	user: Joi.string().required(),
	details: Joi.string(),
	type: Joi.string().required().default('default')
};

module.exports = async(ctx) => {
	const data = ctx.request.body;
	const username = ctx.state.user.username;
	
	const user = await ctx.userService.get(data.user);
	const reciever = await ctx.userService.get(username);
	
	data.user = user._id;
	data.reciever = reciever._id;

	const {
		error,
		value
	} = Joi.validate(data, schema, joiConfig);
	if (error && !username) {
		ctx.status = 404;
		ctx.body = error;
		return;
	}

	const newInvoice = await ctx.payService.create(value);

	ctx.status = 201;
	ctx.body = newInvoice;
};

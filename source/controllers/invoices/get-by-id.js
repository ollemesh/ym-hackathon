'use strict';

module.exports = async(ctx) => {
    const invoicesId = Number(ctx.params.id);
    ctx.body = await ctx.payService.getById(invoicesId);
};

'use strict';

const ApplicationError = require('libs/application-error');

async function prepareData(data) {
	const result = [];
	data.forEach((invoice) => {
		result.push({
			id: invoice.id,
			type: invoice.type,
			status: invoice.status,
			time: invoice.created_at,
			sum: invoice.sum
		});
	}, this);

	return result;
}

module.exports = async (ctx) => {
	if (!ctx.isAuthenticated()) {
		throw new ApplicationError('Access denied', 403);
	}

	const {user} = await ctx.passport;
	const invoices = await ctx.payService.getAll(user._id);
	ctx.body = await prepareData(invoices);
};

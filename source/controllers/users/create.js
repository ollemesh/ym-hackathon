'use strict';

module.exports = async (ctx) => {
	const data = {
		username: ctx.request.body.username,
		password: ctx.request.body.password
	};

	ctx.body = await ctx.userService.create(data);
};

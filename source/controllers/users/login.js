'use strict';

module.exports = async (ctx) => {
	if (ctx.passport.user) {
		ctx.body = ctx.passport.user;
	} else {
		ctx.throw('User not found');
	}
};

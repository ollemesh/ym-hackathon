'use strict';

module.exports = async (ctx) => {
	await ctx.logout();
	ctx.body = {success: true};
};

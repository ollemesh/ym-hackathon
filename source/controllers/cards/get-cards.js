'use strict';

const ApplicationError = require('libs/application-error');

module.exports = async(ctx) => {
	if (!ctx.isAuthenticated()) {
		throw new ApplicationError('Access denied', 403);
	}

	const {user} = ctx.passport;
	ctx.body = await ctx.cardService.getAllByUser(user._id);
};

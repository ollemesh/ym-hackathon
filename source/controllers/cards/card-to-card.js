'use strict';

const ApplicationError = require('libs/application-error');

module.exports = async (ctx) => {
	const cardId = parseInt(ctx.params.id, 10);
	if (!cardId) throw new ApplicationError(`No card with id ${cardId}`, 404);

	const operation = ctx.request.body;
	const data = {
		target: operation.target,
		sum: operation.sum,
		cardId
	}

	const transaction = await ctx.cardService.transfer(data);

	ctx.status = 200;
	ctx.body = transaction;
};

'use strict';

const ApplicationError = require('libs/application-error');

module.exports = async (ctx) => {
	if (!ctx.isAuthenticated()) {
		throw new ApplicationError('Access denied', 403);
	}

	const card = ctx.request.body;
	const {user} = ctx.passport.user;
	card.user = user;

	const newCard = await ctx.cardService.create(card);
	ctx.status = 201;
	ctx.body = newCard;
};

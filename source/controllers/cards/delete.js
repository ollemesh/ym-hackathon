'use strict';

module.exports = async (ctx) => {
	const cardId = Number(ctx.params.id);
	await ctx.cardService.remove(cardId);
	ctx.status = 200;
};

'use strict';

const ApplicationError = require('libs/application-error');

module.exports = async(ctx) => {
	if (!ctx.isAuthenticated()) {
		throw new ApplicationError('Access denied', 403);
	}

	// ctx.body = await ctx.transactionService.getAll(ctx.passport.user._id);
	ctx.body = await ctx.transactionService.getAll();
};

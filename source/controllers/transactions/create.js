'use strict';

const ApplicationError = require('libs/application-error');
const _ = require('lodash');
const Joi = require('joi');
const joiConfig = {
	allowUnknown: true,
	abortEarly: false
}

const postTransactionFields = ['type', 'time', 'sum', 'data'];
const schema = {
	sum: Joi.number().required().positive(),
	data: Joi.string(),
	type: Joi.string().required().allow(['prepaidCard', 'paymentMobile', 'card2Card']),
	time: Joi.date()
};

module.exports = async(ctx) => {
	const transaction = _.pick(ctx.request.body, postTransactionFields);

	const cardId = parseInt(ctx.params.id);
	if (!cardId) throw new ApplicationError(`No card with id ${cardId}`, 404);

	const {
		error,
		value
	} = Joi.validate(transaction, schema, joiConfig);
	if (error) {
		throw new ApplicationError(error, 400);
	}

	value.time = transaction.time || (new Date()).toISOString();

	const card = await ctx.cardService.getCard(cardId);
	if (!card) {
		throw new ApplicationError(`No card with id ${cardId}`, 404);
	}

	const newTransaction = await ctx.transactionService.create(value);
	ctx.status = 201;
	ctx.body = newTransaction;
};

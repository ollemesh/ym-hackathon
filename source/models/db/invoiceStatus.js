const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const InvoiceStatus = mongoose.model('InvoiceStatus', {
	id: {
		type: Number,
		required: true
	},
	name: String
});

module.exports = InvoiceStatus;

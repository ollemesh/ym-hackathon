const mongoose = require('mongoose');

const User = mongoose.model('User', {
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		validate: {
			validator(value) {
				return true;
				// return utils.validateCardNumber(value);
			},
			message: '{VALUE} is not a valid password!'
		},
		select: false,
		required: true
	},
	name: {
		type: String
	},
	role: {
		type: String,
		enum: ['user', 'manager'],
		default: 'user'
	},
	walletNumber: {
		type: Number
	},
});

module.exports = User;

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const invoiceSchema = new Schema({
	id: {
		type: Number,
		required: true
	},
	created_at: {
		type: Date,
		default: Date.now
	},
	updated_at: {
		type: Date,
		default: Date.now
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	reciever: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	type: String,
	details: Schema.Types.Mixed,
	sum: Number,
	status: {
		type: String,
		required: true,
		default: 'New'
	}
	// status: {
	// 	type: mongoose.Schema.Types.ObjectId,
	// 	ref: 'InvoiceStatus'
	// }
});

invoiceSchema.methods.updateStatus = function (status) {
	this.status = status;
};

const Invoice = mongoose.model('Invoice', invoiceSchema, 'invoices');

module.exports = Invoice;

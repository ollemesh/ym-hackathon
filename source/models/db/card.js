const utils = require('../../../libs/utils');
const mongoose = require('mongoose');
const Card = mongoose.model('Card', {
	id: {
		type: Number,
		required: true
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	cardNumber: {
		type: String,
		validate: {
			validator(value) {
				return utils.validateCardNumber(value);
			},
			message: '{VALUE} is not a valid card number!'
		},
		required: [true, 'Card number required']
	},
	balance: {
		type: Number,
		required: true
	},
	isVirtual: { // maybe shouldbe type of card, crypto, debit, credit, virtual etc
		type: Boolean,
		default: false
	}
});

module.exports = Card;

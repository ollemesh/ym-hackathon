'use strict';

const ApplicationError = require('libs/application-error');

const DbModel = require('./common/dbModel');

class Users extends DbModel {
	constructor() {
		super('user');
	}

	/**
	 * Добавляет пользователя
	 *
	 * @param {Object} user данные пользователя
	 * @returns {Promise.<Object>}
	 */
	async create(user) {
		const isDataValid = user
			&& Object.prototype.hasOwnProperty.call(user, 'username')
			&& Object.prototype.hasOwnProperty.call(user, 'password');

		if (isDataValid) {
			await this._insert(user);
			return user;
		}

		throw new ApplicationError('User data is invalid', 400);
	}

	async updateInfo(id, info) {
		await this._update({_id: id}, info);
	}
}

module.exports = Users;

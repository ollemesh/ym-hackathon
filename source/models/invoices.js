'use strict';

const ApplicationError = require('libs/application-error');
const DbModel = require('./common/dbModel');

const status = {
	new: 'New',
	approving: 'Approving',
	approved: 'Approved',
	billing: 'Billing',
	billed: 'Billed',
	canceled: 'Canceled'
};

class Invoices extends DbModel {
	constructor() {
		super('invoice');
		this.status = status;
	}

	async _createTyped(invoice) {
		switch (invoice.type) {
			case 'mutual':
				return this._mutualInvoice(invoice);
				break;
			default:
				return this._defaultInvoice(invoice);
				break;
		}
	}

	async _defaultInvoice(invoice){
		this._insert(invoice);
		return invoice;
	}

	async _mutualInvoice(data) {
		const master = {
			id: await this._generateId(),
			user: data.user,
			sum: data.sum,
			details: {
				users: data.users,
				additional: data.details,
			},
			type: data.type
		}
		await this._insert(master);

		if (data.users.length) {
			const sumChunk = master.sum / data.users.length;
			let id = await this._generateId(); // uglyyyy
			data.users.map(async(val) => {
				await this._insert({
					id: id++,
					user: val,
					reciever: master.user,
					sum: sumChunk,
					details: {
						masterInvoice: master.id
					}
				});
			});
		} else throw new Error('Mutual invoice is not mutual, shuould be with users');
		await this.updateStatus(master, this.status.approving);

		return master;
	}

	async updateStatus(invoice, status) {

		let id;
		if (invoice && typeof invoice === 'object' &&
			invoice.constructor === Object && invoice.id) id = invoice.id;
		else id = Number(invoice);

		await this._update({
			id: id
		}, {
			status: status
		});

		return status;
	}

	async approvePayment(data) {
		let newStatus;
		if (data.isApproved) newStatus = this.status.approved;
		else newStatus = this.status.canceled;

		await this.updateStatus(data.invoiceNumber, newStatus);
		return newStatus;
	}

	async create(invoice) {
		const newInvoice = Object.assign({}, invoice, {
			id: await this._generateId()
		});
		return await this._createTyped(newInvoice);
	}

	async pay(data) {
		await this.updateStatus(data.invoiceNumber, this.status.billing);
	}

	async getByUser(userId) {
		const items = await this.getBy({
			userId
		});
		return items;
	}
}

module.exports = Invoices;

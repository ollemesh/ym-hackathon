const schedule = require('node-schedule');
const transactionService = require('./transactionService');
const cardService = require('./cardService');

const InvoicesModel = require('../models/invoices');

const WAITING_TIME = 2;
async function cancelInvoice(invoice) {
	
	const childInvoices = await invoicesModel.getAllBy({
		'details.masterInvoice': {
			$eq: invoice.id
		},
	});
	
	childInvoices.map((child) => {
		if (child.status == invoicesModel.status.billed) {
			// back transaction
			const data = {
				user: invoice.user,
				reciever: child.user,
				sum: child.sum
			}
			transact(data);
		}
		invoicesModel.updateStatus(child.id, invoicesModel.status.canceled);
	});
	invoicesModel.updateStatus(invoice.id, invoicesModel.status.canceled);	
}

async function cancelInspiredInvoices() {
	const invoices = await invoicesModel.getAllBy({
		type: 'mutual',
		status: {
			$eq: invoicesModel.status.approving
		},
	});

	invoices.map((invoice) => {		
		const min = Math.abs(Date.parse(invoice.created_at) - new Date()) / (60 * 1000);
		if (min > WAITING_TIME) cancelInvoice(invoice);
	});
}

async function transact(invoice) {
	const owner = await cardService.getByUser(invoice.user);
	const target = await cardService.getByUser(invoice.reciever);

	const data = {
		target: target.id,
		sum: invoice.sum,
		cardId: owner.id
	}
	return await cardService.transfer(data);
}


const removeInspiredJob = schedule.scheduleJob('*/5 * * * * *', cancelInspiredInvoices);
const invoicesModel = new InvoicesModel();
module.exports = {

	async create(data) {
		return invoicesModel.create(data);
	},

	async getAll(userId) {
		return invoicesModel.getAllBy({user: userId});
	},

	async getById(invoiceId) {
		return invoicesModel.getBy({id: invoiceId});
	},

	async isMutualApproved(invoiceNumber) {
		const child = await invoicesModel.get(invoiceNumber);
		const master = child.details.masterInvoice;

		const childInvoices = await invoicesModel.getAllBy({
			'details.masterInvoice': {
				$eq: master
			},
		});

		let isCanceled = false;
		let isApproved = true;
		childInvoices.forEach(function (inv) {
			isApproved = isApproved && inv.status == invoicesModel.status.billed;
			isCanceled = isCanceled || inv.status == invoicesModel.status.canceled;
		}, this);
		if(isCanceled) {
			const mutual = await invoicesModel.get(master);
			cancelInvoice(mutual);
		}
		else if (isApproved )
			invoicesModel.updateStatus(master, invoicesModel.status.approved);

	},

	async payInvoice(data) {
		const invoice = await invoicesModel.get(data.invoiceNumber);
		if(invoice.status != invoicesModel.status.new) throw new Exception('Payment is not alowed');
		if (await invoicesModel.approvePayment(data) == invoicesModel.status.approved) {
			invoicesModel.pay();
			const transaction = await transact(invoice);
			return await invoicesModel.updateStatus(invoice.id, invoicesModel.status.billed);
		}
		return invoicesModel.status.canceled;
	}
};

const TransactionsModel = require('../models/transactions');

const transactionsModel = new TransactionsModel();
module.exports = {
	async getAll(user) {
		return await transactionsModel.getAllBy({
			user
		});
	},

	async getByCard(cardId) {
		return await transactionsModel.getByCard(cardId);
    },

    async create(data) {
        return  await transactionsModel.create(data);
    }
}

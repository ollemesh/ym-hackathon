const CardsModel  = require('../models/cards');
const transactionService = require('./transactionService');

const cardsModel = new CardsModel();
module.exports = {

	async create(card) {
		return await cardsModel.create(card);
	},

	async remove(id) {
		await cardsModel.remove(id);
	},


	async getAll() {
		return await cardsModel.getAll();
	},

	async getAllByUser(user) {
		return await cardsModel.getAllBy({
			isVirtual: false,
			user
		});
	},

	async getCard(id) {
		return await cardsModel.get(id);
	},

	async getByUser(user, isVirtual = true) {
		return await cardsModel.getBy({
			user,
			isVirtual
		});
	},

    async transfer(data) {
		const {
			target,
			sum,
			cardId
		} = data;

		await cardsModel.withdraw(cardId, sum);
		await cardsModel.refill(target, sum);

		const sourceCard = await this.getCard(cardId);
		const targetCard = await this.getCard(target);

		const transaction = await transactionService.create({
			cardId: sourceCard.id,
			type: 'card2Card',
			data: {
				cardNumber: targetCard.cardNumber
			},
			time: new Date().toISOString(),
			sum
		});

		return transaction;
	},

	async payMobile(data) {
		const {
			sum,
			phoneNumber,
			cardId,
			commission
		} = data;

		cardsModel.withdraw(cardId, parseInt(sum, 10) + commission);

		const transaction = await transactionService.create({
			cardId,
			type: 'paymentMobile',
			data: {
				phoneNumber
			},
			time: new Date().toISOString(),
			sum
		});
	},

	async fillCardByMobile(data) {

		const {
			sum,
			phoneNumber,
			cardId
		} = data;

		ctx.cardsModel.withdraw(cardId, parseInt(sum, 10) + commission);

		const transaction = await transactionService.create({
			cardId,
			type: 'prepaidCard',
			data: {
				phoneNumber
			},
			time: new Date().toISOString(),
			sum
		});
	}
}

const cardService = require('./cardService');

module.exports = (model) => {
	const usersModel = model;
	return {

		async create(data) {
			const newUser = await usersModel.create({
				username: data.username,
				password: data.password,
				walletNumber: null
			});
			const user = await usersModel.getBy({username: data.username});
			if (!user) throw new Error('User is not created');

			const wallet = await cardService.create({
				cardNumber: 0,
				balance: 0,
				isVirtual: true,
				user: user._id
			});

			await usersModel.updateInfo(user._id, {walletNumber: wallet.id});
			user.walletNumber = wallet.id;
			return newUser;
		},

		async get(username) {
			return usersModel.getBy({username: username});
		}
	}

}

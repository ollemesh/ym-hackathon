const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BabelWebpackPlugin = require('babel-minify-webpack-plugin');
const path = require('path');

const PROD = process.env.NODE_ENV === 'production';
const BUILD_PATH = path.join(__dirname, 'public');

const commonLoaders = [{
	test: /\.jsx?$/,
	use: 'babel-loader',
	exclude: /node_modules/,
}];

const configClient = {
	name: 'client',
	entry: './source/views/index.src.js',
	output: {
		path: BUILD_PATH,
		filename: 'index.js',
		publicPath: '/',
	},
	module: {
		rules: commonLoaders.concat([
			{
				test: /\.css$/,
				use: PROD ? ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: 'css-loader',
				}) : ['style-loader', 'css-loader'],
			},
		]),
	},
	plugins: [],
};

if (!PROD) {
	configClient.entry = [
		'webpack-hot-middleware/client?name=client',
		'react-hot-loader/patch',
	].concat(configClient.entry);

	configClient.plugins = configClient.plugins.concat([
		new webpack.HotModuleReplacementPlugin(),
	]);
}

if (PROD) {
	configClient.plugins = configClient.plugins.concat([
		new ExtractTextPlugin('index.css'),
		new BabelWebpackPlugin(),
		new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en-gb|ru/),
		new webpack.optimize.ModuleConcatenationPlugin(),
	]);
}

const configServer = {
	name: 'server',
	target: 'node',
	externals: [nodeExternals({whitelist: [/^antd/]})],
	entry: [
		'babel-polyfill',
		'./source/views/index.server.src.js',
	],
	output: {
		path: BUILD_PATH,
		filename: 'index.server.js',
		publicPath: '/',
		libraryTarget: 'commonjs2',
	},
	module: {
		rules: commonLoaders.concat([
			{
				test: /\.css$/,
				use: 'null-loader',
			},
		]),
	},
	plugins: [],
};

module.exports = [configClient, configServer];

/* eslint no-console: "off" */
const getWSUrl = () => {
	const loc = window.location;
	const protocol = loc.protocol === 'https:' ? 'wss:' : 'ws:';
	return `${protocol}//${loc.host}/`;
};

const serviceMessages = {
	userDisconnected: 'Пользователь потерял связь с сервером'
};
const usersEl = document.getElementById('users');
const messageEl = document.getElementById('message');
const iframeEl = document.getElementById('userScreen');
const iframeWin = iframeEl.contentWindow;
const iframeDoc = iframeWin.document;

const MORPHDOM_PATH = 'vendor/morphdom-umd.min.js';
const ADMIN_PASSWORD = 'W0uyXZ5zLB4hVJ8dSM4h';
const ws = new WebSocket(`${getWSUrl()}?password=${ADMIN_PASSWORD}`);
let selectedUser;

const getScrollbarSize = () => {
	// Create the measurement node
	const scrollDiv = document.createElement('div');
	scrollDiv.style.width = '100px';
	scrollDiv.style.height = '100px';
	scrollDiv.style.overflow = 'scroll';
	scrollDiv.style.position = 'absolute';
	scrollDiv.style.top = '-9999px';
	document.body.appendChild(scrollDiv);

	// Get the scrollbar width
	const scrollbarSize = {
		width: scrollDiv.offsetWidth - scrollDiv.clientWidth,
		height: scrollDiv.offsetHeight - scrollDiv.clientHeight
	};

	// Delete the DIV
	document.body.removeChild(scrollDiv);
	return scrollbarSize;
};

const injectDependencies = () => {
	const script = document.createElement('script');
	script.src = MORPHDOM_PATH;
	iframeDoc.body.appendChild(script);
};

const selectUser = (userId) => {
	selectedUser = userId;
	ws.send(JSON.stringify({
		type: 'ADMIN_SUBSCRIBE_TO_USER',
		data: userId
	}));
};

const updateUserList = (users) => {
	usersEl.innerHTML = '';
	Object.keys(users).forEach((userId) => {
		const user = users[userId];
		const userEl = document.createElement('div');
		userEl.className = 'user-list__user';
		userEl.textContent = `${user.name} (${userId.slice(0, 8)})`;
		userEl.addEventListener('click', selectUser.bind(null, userId));
		usersEl.appendChild(userEl);
	});
};

const checkUserDisconnect = (users) => {
	if (selectedUser && !users[selectedUser]) {
		messageEl.textContent = serviceMessages.userDisconnected;
		iframeEl.style.display = 'none';
		selectedUser = null;
	}
};

const setScreenSize = (height, width) => {
	const {
		clientHeight: operatorHeight,
		clientWidth: operatorWidth
	} = document.documentElement;
	const scale = Math.min(
		operatorHeight / height,
		operatorWidth / width
	);

	const {style} = iframeEl;
	iframeEl.height = `${height}`;
	iframeEl.width = `${width}`;
	style.transform = `scale(${scale})`;
	style.left = `${(operatorWidth - width) / 2}px`;
	style.top = `${(operatorHeight - height) / 2}px`;
};

const setupUserScreen = (data) => {
	let {height, width} = data.screenSize;
	const {hScroll, vScroll} = data.screenSize;
	const scrollbarSize = getScrollbarSize();
	if (vScroll) { width += scrollbarSize.width; }
	if (hScroll) { height += scrollbarSize.height; }
	setScreenSize(height, width);

	messageEl.textContent = '';
	iframeEl.style.display = 'block';
	iframeDoc.documentElement.innerHTML = data.html;
};

// Cursor tracking
let cursorEl;
let clickAreaEl;
let cursorX = 0;
let cursorY = 0;
let mouseDown = false;

const restoreCursor = () => {
	if (!cursorEl) {
		// Create new cursor icon
		cursorEl = iframeDoc.createElement('img');
		cursorEl.style.width = '30px';
		cursorEl.style.height = '30px';
		cursorEl.style.position = 'absolute';
		cursorEl.style.zIndex = '2';
		cursorEl.src = 'assets/cursor.svg';
		cursorEl.style.left = `${cursorX}px`;
		cursorEl.style.top = `${cursorY}px`;
		iframeDoc.body.appendChild(cursorEl);

		// Create click area
		clickAreaEl = iframeDoc.createElement('div');
		clickAreaEl.style.width = '101px';
		clickAreaEl.style.height = '101px';
		clickAreaEl.style.position = 'absolute';
		clickAreaEl.style.zIndex = '1';
		clickAreaEl.style.borderRadius = '50%';
		clickAreaEl.style.left = `${cursorX - 45}px`;
		clickAreaEl.style.top = `${cursorY - 42}px`;
		clickAreaEl.style.transition = mouseDown ? 'none' : 'background-color 0.5s';
		clickAreaEl.style.backgroundColor = mouseDown ? 'rgba(255, 255, 0, 0.4)' : 'rgba(0, 0, 0, 0)';
		iframeDoc.body.appendChild(clickAreaEl);
	} else if (cursorEl.parentNode !== iframeDoc.body) {
		iframeDoc.body.appendChild(cursorEl);
		iframeDoc.body.appendChild(clickAreaEl);
	} else {
		cursorEl.style.left = `${cursorX}px`;
		cursorEl.style.top = `${cursorY}px`;
		clickAreaEl.style.left = `${cursorX - 45}px`;
		clickAreaEl.style.top = `${cursorY - 42}px`;
		clickAreaEl.style.transition = mouseDown ? 'none' : 'background-color 0.5s';
		clickAreaEl.style.backgroundColor = mouseDown ? 'rgba(255, 255, 0, 0.4)' : 'rgba(0, 0, 0, 0)';
	}
};

const updateUserScreen = (data) => {
	iframeWin.morphdom(iframeDoc.documentElement, data.html);
	restoreCursor();
};

const updateCursorPosition = ({x, y}) => {
	cursorX = x;
	cursorY = y;
	restoreCursor();
};

const showClickArea = () => {
	mouseDown = true;
	restoreCursor();
};

const hideClickArea = () => {
	mouseDown = false;
	restoreCursor();
};

const applyDocumentScroll = (data) => {
	// mousemove event is not dispatched after scroll:
	// shift the cursor accordingly.
	const {x, y} = data;
	const {pageXOffset, pageYOffset} = iframeWin;
	console.log(x, pageXOffset);
	console.log(y, pageYOffset);
	cursorX += x - pageXOffset;
	cursorY += y - pageYOffset;
	restoreCursor();
	iframeWin.scroll(x, y);
};

const applyElementScroll = (data) => {
	const {selector, x, y} = data;
	const el = iframeDoc.querySelector(selector);
	el.scrollLeft = x;
	el.scrollTop = y;
};

const onMessage = (event) => {
	const {type, data} = JSON.parse(event.data);
	switch (type) {
		case 'ADMIN_USER_LIST':
			updateUserList(data);
			checkUserDisconnect(data);
			break;
		case 'USER_SCREEN_INITIAL':
			setupUserScreen(data);
			break;
		case 'USER_SCREEN_UPDATE':
			updateUserScreen(data);
			break;
		case 'USER_MOUSE_MOVE':
			updateCursorPosition(data);
			break;
		case 'USER_MOUSE_DOWN':
			showClickArea();
			break;
		case 'USER_MOUSE_UP':
			hideClickArea();
			break;
		case 'USER_SCROLL_DOCUMENT':
			applyDocumentScroll(data);
			break;
		case 'USER_SCROLL_ELEMENT':
			applyElementScroll(data);
			break;
		default:
			console.error(`Messages of type ${type} are not supported`);
	}
};

// Initialize
injectDependencies();
ws.addEventListener('message', console.log);
ws.addEventListener('message', onMessage);
ws.addEventListener('open', () => console.log('Connected'));
ws.addEventListener('close', () => console.log('Disconnected'));
ws.addEventListener('error', console.error);
